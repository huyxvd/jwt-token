﻿using System.Security.Cryptography;
using System.Text;

string headerJson = "{\"alg\":\"HS256\",\"typ\":\"JWT\"}";
string payloadJson = "{\"sub\":\"1234567890\",\"name\":\"John Doe\",\"role\":\"admin\"}";

string base64UrlHeader = Base64UrlEncode(Encoding.UTF8.GetBytes(headerJson));
string base64UrlPayload = Base64UrlEncode(Encoding.UTF8.GetBytes(payloadJson));

Console.WriteLine($"base64UrlHeader: {base64UrlHeader}");
Console.WriteLine($"base64UrlPayload: {base64UrlPayload}");

string secretKey = "01234567890123456789012345678912"; // // 256 bit hoặc 32 byte
string signature = Signature(base64UrlHeader, base64UrlPayload, secretKey);

Console.WriteLine($"signature: {signature}");
Console.WriteLine($"jwt: {base64UrlHeader}.{base64UrlPayload}.{signature}");

string decodedHead = Base64UrlDecode(base64UrlHeader);
string decodedPayload = Base64UrlDecode(base64UrlPayload);

Console.WriteLine($"decodedHead: {decodedHead}");
Console.WriteLine($"decodedPayload: {decodedPayload}");

string Base64UrlEncode(byte[] data)
{
    string base64 = Convert.ToBase64String(data);
    return base64.Replace('+', '-').Replace('/', '_').TrimEnd('=');
}


string Signature(string base64UrlHeader, string base64UrlPayload, string secretKey)
{
    byte[] key = Encoding.UTF8.GetBytes(secretKey);
    HMACSHA256 hmac = new HMACSHA256(key);
    byte[] headerPayloadInbyte = Encoding.UTF8.GetBytes($"{base64UrlHeader}.{base64UrlPayload}");
    byte[] signatureBytes = hmac.ComputeHash(headerPayloadInbyte);
    string signature = Base64UrlEncode(signatureBytes);
    return signature;
}

string Base64UrlDecode(string base64Url)
{
    string base64 = base64Url.Replace('-', '+').Replace('_', '/');

    // Thêm padding nếu cần
    int mod4 = base64.Length % 4;
    if (mod4 > 0)
    {
        base64 += new string('=', 4 - mod4);
    }
    byte[] bytes = Convert.FromBase64String(base64);
    return Encoding.UTF8.GetString(bytes);
}